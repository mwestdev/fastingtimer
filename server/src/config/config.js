module.exports = {
    port: process.env.PORT || 8081,
    db: {
        database: process.env.DB_NAME || 'fastingtimer',
        user: process.env.DB_USER || 'fastingtimer',
        password: process.env.DB_PASS || 'fastingtimer',
        options: {
            dialect: process.env.DIALECT || 'sqlite',
            host: process.env.HOST || 'localhost',
            storage: './fastingtimer.sqlite',
        },
    },
    authentication: {
        jwtSecret: process.env.JWT_SECRET || 'secret',
    },
}
